﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace TestNetworkApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<TestItem> TestedItems { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            TestedItems = new List<TestItem>();
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            List<TestItem> tests = new List<TestItem>();

            spContent.Children.Clear();
            TestedItems.Clear();

            tests.Add(new TestItem(
                "Kiosk Middleware", 
                "Heartbeat service and VMS content is handled via the middleware server", 
                "https://kiosk.veii.com/api/ping", 
                false));

            tests.Add(new TestItem(
                "USAT API",
                "Processes credit card and loyalty card transactions to USAT",
                "https://ec.usatech.com:9443/soap/ec2",
                false));// ec.usatech.com:9443/soap/ec2

            tests.Add(new TestItem(
                "USAT Loyalty",
                "Handles new loyalty card sign ups",
                "https://getmore.usatech.com/soap/ps",
                false
                ));

            tests.Add(new TestItem(
                "USAT Portal",
                "Used to configure credit card readers with USAT",
                "https://usalive.usatech.com",
                false
                ));

            tests.Add(new TestItem(
                "Mail Server",
                "Used to send alerts and receipts",
                "https://api.mailgun.net/v3",
                false
                ));

            tests.Add(new TestItem(
                "Bogus Server",
                "Used to things that like to just break",
                "https://www.google.com/justdie",
                false
                ));


            foreach (var x in tests)
            {
                TestEnpoint(x);
            }
        }

        private async void TestEnpoint(TestItem item)
        {
            await Task.Factory.StartNew(() =>
            {
                try
                {
                    using (WebClient client = new WebClient())
                    {
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        client.DownloadString(item.Address.ToString());
                        item.PassedTest = true;
                    }
                }
                catch (Exception ex)
                {
                    item.PassedTest = false;
                    item.Description = ex.Message;
                }
            });

            TestedItems.Add(item);
            UpdateList();
        }

        private void UpdateList()
        {
            spContent.Children.Clear();

            foreach (var x in TestedItems)
            {
                spContent.Children.Add(new TestedEndpoint(x.Name, x.Description, x.PassedTest));
            }
        }
    }

    public class TestItem
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public bool PassedTest { get; set; }

        public TestItem() { }

        public TestItem(string name, string description, string address, bool passed)
        {
            Name = name;
            Description = description;
            Address = address;
            PassedTest = passed;
        }
    }
}
