﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestNetworkApp
{
    /// <summary>
    /// Interaction logic for TestedEndpoint.xaml
    /// </summary>
    public partial class TestedEndpoint : UserControl
    {
        private ImageBrush PassedImg, FailedImg, InfoImg;
        private string Description = string.Empty;

        public TestedEndpoint(string name, string description, bool passed)
        {
            InitializeComponent();

            PassedImg = new ImageBrush();
            PassedImg.ImageSource = new BitmapImage(new Uri(@"pack://application:,,,/" +
                Assembly.GetExecutingAssembly().GetName().Name + ";component/Images/OK.png", UriKind.Absolute));

            FailedImg = new ImageBrush();
            FailedImg.ImageSource = new BitmapImage(new Uri(@"pack://application:,,,/" +
                Assembly.GetExecutingAssembly().GetName().Name + ";component/Images/Block.png", UriKind.Absolute));

            InfoImg = new ImageBrush();
            InfoImg.ImageSource = new BitmapImage(new Uri(@"pack://application:,,,/" +
                Assembly.GetExecutingAssembly().GetName().Name + ";component/Images/Info.png", UriKind.Absolute));

            tbName.Text = name;
            Description = description;
            btnInfo.Background = InfoImg;

            if (passed)
            {
                lblImage.Background = PassedImg;
            }
            else
            {
                lblImage.Background = FailedImg;
            }
        }

        private void btnInfo_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(Description);
        }
    }
}
